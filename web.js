'use strict';

var gzippo = require('gzippo');
var express = require('express');

var nodeApp = express();

nodeApp.use(gzippo.staticGzip('' + __dirname + '/app'));
nodeApp.listen(process.env.PORT || 5000);